"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryModel = exports.ProductModel = void 0;
const ProductModel_1 = __importDefault(require("./ProductModel"));
exports.ProductModel = ProductModel_1.default;
const DeliveryModel_1 = __importDefault(require("./DeliveryModel"));
exports.DeliveryModel = DeliveryModel_1.default;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ProductModel {
    constructor(product) {
        this.productId = product.productId;
        this.productName = product.productName;
        this.productPrice = product.productPrice;
    }
}
exports.default = ProductModel;

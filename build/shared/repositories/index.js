"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductRepository = void 0;
const ProductRepository_1 = __importDefault(require("./ProductRepository"));
exports.ProductRepository = ProductRepository_1.default;

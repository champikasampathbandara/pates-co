"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const promotions_json_1 = __importDefault(require("../rules/promotions.json"));
class Promotion {
    constructor(limit) {
        this._limit = limit;
    }
    /**
     *
     * @param strategy
     * @param args
     * @private
     *
     * this function extend the capability of promotions to have more granular level rules for products
     */
    invokeStrategyFunction(strategy, ...args) {
        switch (strategy) {
            case "buyOneAndGetSecond":
                this.buyOneAndGetSecond(args[0], args[1]);
                break;
        }
    }
    calculateSingleProductBasedDiscount() {
        const strategy = promotions_json_1.default.product_based.single_product.strategy;
        const discountPercent = promotions_json_1.default.product_based.single_product.discount_percent;
        this.invokeStrategyFunction(strategy, discountPercent);
        return 0;
    }
    //This method is for demo purpose only. will not be implemented
    calculateComboProductBasedDiscount() {
        return 0;
    }
    //This method is for demo purpose only. will not be implemented
    calculateTotalBasedDiscount() {
        return 0;
    }
    calculateTotalPromotionalDiscount(cartItems) {
        this._cartItems = cartItems;
        let discount = this.calculateSingleProductBasedDiscount();
        discount += this.calculateTotalBasedDiscount();
        return discount;
    }
    //TODO: assumed there are no max discount limit
    buyOneAndGetSecond(productId, discount) {
        var _a;
        const discountedProduct = (_a = this._cartItems) === null || _a === void 0 ? void 0 : _a.find((cartItem) => cartItem.productId === productId);
        let discountInCurrency = discountedProduct ? ((discountedProduct === null || discountedProduct === void 0 ? void 0 : discountedProduct.count) * (discountedProduct === null || discountedProduct === void 0 ? void 0 : discountedProduct.count))
            - (Math.trunc(discountedProduct === null || discountedProduct === void 0 ? void 0 : discountedProduct.count) * discount / 100) : 0;
        return discountInCurrency;
    }
}
exports.default = Promotion;

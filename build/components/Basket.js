"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Product_1 = __importDefault(require("./Product"));
class Basket {
    constructor(shipping, promotion) {
        this._delivery = shipping;
        this._promotion = promotion;
    }
    add(productId) {
        var _a, _b;
        let count = 0;
        count = ((_a = this._products) === null || _a === void 0 ? void 0 : _a.findIndex(value => value.productId === productId)) ? 1 : count++;
        (_b = this._products) === null || _b === void 0 ? void 0 : _b.push({
            productId: productId,
            count: count,
            productObj: new Product_1.default(productId)
        });
    }
    //buy one red plate get second for half price
    //fullPricePlates = Math.ceil((numOfR1Plates/2 ));
    //Assume offers cannot be combined with another offer
    calculateTotal() {
    }
    getAddedProducts() {
        return this._products;
    }
}
exports.default = Basket;

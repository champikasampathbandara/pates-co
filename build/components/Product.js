"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const repositories_1 = require("../shared/repositories");
const models_1 = require("../shared/models");
class Product {
    constructor(productId) {
        this._repository = new repositories_1.ProductRepository();
        const productInfo = this._repository.getProductInfoById(productId);
        this.productId = productId;
        this.productName = productInfo.productName;
        this.productPrice = productInfo.productPrice;
    }
    createProduct(productInfo) {
        this._model = new models_1.ProductModel({
            productId: this.productId,
            productName: productInfo.productName,
            productPrice: productInfo.productPrice
        });
        this._repository.create(this._model);
    }
}
exports.default = Product;

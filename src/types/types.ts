export type ProductType = {
  productId : string;
  productName: string,
  productPrice: number
}

export type DeliveryRulesToTotalType = {
  total : {
    less_than? : Array<any>,
    greater_than? : Array<any>
  }
}

export type CartItemType = {
  productId : string,
  count : number,
  productObj : ProductType
}
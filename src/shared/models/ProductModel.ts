import {ProductType} from "../../types/types";

export default class ProductModel {
  public readonly productId : string;
  public readonly productName : string;
  public readonly productPrice : number;

  constructor(product: ProductType) {
    this.productId = product.productId;
    this.productName = product.productName;
    this.productPrice = product.productPrice;
  }
}
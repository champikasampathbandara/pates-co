import Product from "./Product";
import promotions from "../rules/promotions.json";
import {CartItemType } from "../types/types";

export default class Promotion {
  private _cartItems: Array<CartItemType> | undefined;
  private _limit : number;

  constructor(limit: number) {
    this._limit = limit;
  }

  /**
   *
   * @param strategy
   * @param args
   * @private
   *
   * this function extend the capability of promotions to have more granular level rules for products
   */
  private invokeStrategyFunction(strategy: string, ...args:any) {
    switch (strategy) {
      case "buyOneAndGetSecond":
        this.buyOneAndGetSecond(args[0], args[1]);
        break;
    }
  }

  private calculateSingleProductBasedDiscount() : number {
    const strategy = promotions.product_based.single_product.strategy;
    const discountPercent = promotions.product_based.single_product.discount_percent;
    this.invokeStrategyFunction(strategy, discountPercent);
    return 0;
  }

  //This method is for demo purpose only. will not be implemented
  private calculateComboProductBasedDiscount() : number {
    return 0;
  }

  //This method is for demo purpose only. will not be implemented
  private calculateTotalBasedDiscount() : number {
    return 0
  }

  public calculateTotalPromotionalDiscount(cartItems : Array<CartItemType>) : number {
    this._cartItems = cartItems;
    let discount:number = this.calculateSingleProductBasedDiscount();
    discount += this.calculateTotalBasedDiscount();
    return discount;
  }

  //TODO: assumed there are no max discount limit
  private buyOneAndGetSecond(productId: string, discount : number) : number {
    const discountedProduct: CartItemType | undefined = this._cartItems?.find(
      (cartItem: CartItemType) => cartItem.productId === productId);
    let discountInCurrency : number = discountedProduct ? (discountedProduct?.count * discountedProduct?.count)
      - (Math.trunc(discountedProduct?.count) * discount / 100) : 0;
    return discountInCurrency;
  }
}
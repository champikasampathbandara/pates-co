import Promotion from "./Promotion";
import Product from "./Product";
import Delivery from "./Delivery";

export default class Basket {

  private _products: Array<{ productId: string; count: number; productObj: Product; }> | undefined;
  private _delivery: Delivery;
  private _promotion: Promotion;

  constructor(shipping: Delivery, promotion: Promotion) {
    this._delivery = shipping;
    this._promotion = promotion
  }

  add(productId : string): void {
    let count = 0;
    count = this._products?.findIndex(value => value.productId === productId) ? 1 : count++;
    this._products?.push({
      productId: productId,
      count: count,
      productObj:new Product(productId) }
    );
  }

  //buy one red plate get second for half price
  //fullPricePlates = Math.ceil((numOfR1Plates/2 ));
  //Assume offers cannot be combined with another offer
  calculateTotal() {

  }

  getAddedProducts() {
    return this._products;
  }

}
import { ProductRepository } from "../shared/repositories";
import { ProductModel } from "../shared/models";
import { ProductType } from "../types/types";

export default class Product {

  /**
   * making the properties public but readonly so from anywhere it is readable but not immutable. this will make my life easier when
   * access product info from other properties
   */
  public readonly productId: string;
  public readonly productName :string;
  public readonly productPrice :number;

  private _repository: ProductRepository
  private _model: ProductModel | undefined

  constructor(productId : string) {
    this._repository = new ProductRepository();

    const productInfo: ProductType = this._repository.getProductInfoById(productId);

    this.productId = productId;
    this.productName = productInfo.productName;
    this.productPrice = productInfo.productPrice;
  }

  public createProduct(productInfo: ProductType) {
    this._model = new ProductModel({
      productId : this.productId,
      productName: productInfo.productName,
      productPrice: productInfo.productPrice
    });
    this._repository.create(this._model);
  }
}

